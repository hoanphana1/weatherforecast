package com.example.weatherforecast.task;

public enum TaskResult {
    SUCCESS, BAD_RESPONSE, IO_EXCEPTION, TOO_MANY_REQUEST
}
