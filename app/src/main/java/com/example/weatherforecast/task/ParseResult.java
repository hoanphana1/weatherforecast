package com.example.weatherforecast.task;

public enum ParseResult {
    PARSED, JSON_EXCEPTION, CITY_NOT_FOUND
}
