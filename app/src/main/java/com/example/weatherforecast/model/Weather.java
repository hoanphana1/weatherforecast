package com.example.weatherforecast.model;

import android.content.Context;

import com.example.weatherforecast.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Weather {
    String city;
    String country;
    Date date;
    String temperature;
    String describtion;
    String wind;
    Double windDirectionDec;
    String pressure;
    String humidity;
    String rain;
    String id;
    String icon;
    String lastUpdate;
    Date sunrise;
    Date sunset;
    double lat;
    double lon;
    double UV;

    public enum Windirection{

        NORTH, NORTH_NORTH_EAST, NORTH_EAST, EAST_NORTH_EAST,
        EAST, EAST_SOUTH_EAST, SOUTH_EAST, SOUTH_SOUTH_EAST,
        SOUTH, SOUTH_SOUTH_WEST, SOUTH_WEST, WEST_SOUTH_WEST,
        WEST, WEST_NORTH_WEST, NORTH_WEST, NORTH_NORTH_WEST;

        public static Windirection byDegree(double degree) {
            return byDegree(degree,Windirection.values().length);
        }

        public static Windirection byDegree(double degree,int numberOfDirections) {
            Windirection[] directions= Windirection.values();
            int availableNumofDirections = directions.length;

            int direction = WindDirectionDegreeToIndex(degree,numberOfDirections)
                    *availableNumofDirections/numberOfDirections;
            return directions[direction];
        }

        public String getlocalizedString (Context context) {
            return context.getResources().getStringArray(R.array.windDirections)[ordinal()];
        }

        public String getArrow (Context context) {
            return context.getResources().getStringArray(R.array.windDirectionArrows)[ordinal() /2 ];
        }
    }

    public Windirection getWindDirection() {
        return Windirection.byDegree(windDirectionDec);
    }


    public Windirection getWindDirection(int numberOfDirections) {
        return Windirection.byDegree(windDirectionDec, numberOfDirections);
    }

    public static int WindDirectionDegreeToIndex(double degree, int numOfDirections) {
        degree = degree%360;

        if (degree<0) degree+=360;

        degree = degree + (180/numOfDirections);


        int direction = (int) Math.floor(degree * numOfDirections/360);
        return direction % numOfDirections;
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDate (String date) {
        try {
            setDate(new Date(Long.parseLong(date)*1000));
        }
        catch (Exception e) {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.ENGLISH);
            try{
                setDate(simpleDateFormat.parse(date));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }

    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getDescribtion() {
        return describtion;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public Double getWindDirectionDec() {
        return windDirectionDec;
    }

    public void setWindDirectionDec(Double windDirectionDec) {
        this.windDirectionDec = windDirectionDec;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getRain() {
        return rain;
    }

    public void setRain(String rain) {
        this.rain = rain;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getSunrise() {
        return sunrise;
    }
    public void setSunrise(Date sunrise){
        this.sunrise= sunrise;
    }

    public void setSunrise(String sunrise) {
        try {
            this.sunrise = new Date(Long.parseLong(sunrise)*1000);
        } catch (Exception e) {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            try {
                setSunrise(simpleDateFormat.parse(sunrise));
            } catch (ParseException ex) {
                setSunrise(new Date());
                ex.printStackTrace();
            }
        }
    }

    public Date getSunset() {
        return sunset;
    }

    public void setSunset(Date sunset) {
        this.sunset = sunset;
    }

    public void setSunset(String sunset){
        try {
            this.sunset = new Date(Long.parseLong(sunset)*1000);

        } catch (Exception e) {
          SimpleDateFormat simpleDateFormat=  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
          try {
              setSunset(simpleDateFormat.parse(sunset));
          } catch (ParseException ex) {
              setSunset(new Date());
              ex.printStackTrace();
          }
        }
    }
    public boolean isWindDirectionAvailable() {
        return windDirectionDec != null;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getUV() {
        return UV;
    }

    public void setUV(double UV) {
        this.UV = UV;
    }
}
