package com.example.weatherforecast.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.LauncherActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.PrecomputedText;
import android.util.JsonReader;
import android.util.Log;
import android.widget.TextView;

import com.db.chart.model.Bar;
import com.db.chart.model.BarSet;
import com.db.chart.model.ChartSet;
import com.db.chart.model.LineSet;
import com.db.chart.util.Tools;
import com.db.chart.view.BarChartView;
import com.db.chart.view.ChartView;
import com.db.chart.view.LineChartView;
import com.example.weatherforecast.R;
import com.example.weatherforecast.constant.Constants;
import com.example.weatherforecast.model.Weather;
import com.example.weatherforecast.task.ParseResult;
import com.example.weatherforecast.utils.Format;
import com.example.weatherforecast.utils.UnitConvertor;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class GraphActivity extends AppCompatActivity {

    SharedPreferences sp;
    ArrayList<Weather> weatherArrayList =new ArrayList<>();

   Paint paint = new Paint() {{
       setStyle(Style.STROKE);
       setAntiAlias(true);
       setPathEffect(new DashPathEffect(new float[]{10,10},0));
       setStrokeWidth(1);
   }};
    //get name date in week ("E")
   SimpleDateFormat dateFormat = new SimpleDateFormat("E")  {{
       setTimeZone(TimeZone.getDefault());
   }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        init();

        String lastLongTerm = sp.getString(Constants.sp_lastLongterm,"");

        if (parseLongtermJson(lastLongTerm) ==ParseResult.PARSED) {
            CreateTempGraph();
            CreateHumidGraph();
            CreatePressureGraph();
            CreateWindGraph();
            CreateRainGraph();
        }
        else {
            Snackbar.make(findViewById(android.R.id.content),R.string.msg_err_parsing_json,Snackbar.LENGTH_LONG).show();
        }
    }

    @SuppressLint({"ResourceAsColor", "SetTextI18n"})
    private void CreateTempGraph () {
        TextView txtTemp = findViewById(R.id.txtTempGraph);
        txtTemp.setText(getResources().getString(R.string.temp) + " ("+ sp.getString(Constants.sp_unit,Constants.sp_default_unit)+")");

        float minTemp = UnitConvertor.convertTemp(Float.parseFloat(weatherArrayList.get(0).getTemperature()),sp);
        float maxTemp = minTemp;
        LineSet lineSet = new LineSet();
        for (int i=0;i<weatherArrayList.size();i++) {
            float temp = UnitConvertor.convertTemp(Float.parseFloat(weatherArrayList.get(i).getTemperature()),sp);
            //find max,min
            minTemp = Math.min(temp,minTemp);
            maxTemp = Math.max(temp,maxTemp);
            lineSet.addPoint(getXLabel(weatherArrayList.get(i),i),temp);
        }
        lineSet.setSmooth(true);
        lineSet.setColor(getResources().getColor(R.color.temp_line));
        lineSet.setThickness(5);

        //(max-min)/4
        int step = Math.max(1,(int)Math.ceil(Math.abs(maxTemp-minTemp)/4));

        LineChartView lineChartView= findViewById(R.id.graph_temp_linechart);
        initLineChar(lineChartView,lineSet,minTemp,maxTemp, step);
    }

    private void initLineChar (ChartView lineChartView, ChartSet lineSet, float min, float max, int step){
            lineChartView.addData((lineSet));
            if (lineChartView instanceof BarChartView) {
                lineChartView.setGrid((int) (max/step),1,paint);
                lineChartView.setXAxis(false);
                lineChartView.setYAxis(false);
                lineChartView.setBorderSpacing((int) Tools.fromDpToPx(10));
            }
            else {
                lineChartView.setGrid(4,1,paint);
                lineChartView.setBorderSpacing((int) Tools.fromDpToPx(1));
            }
            lineChartView.setAxisBorderValues((float) Math.floor(min), (float) Math.ceil(max));
            lineChartView.setStep(step);
            lineChartView.show();
    }

    @SuppressLint("SetTextI18n")
    private void CreateHumidGraph () {
        TextView txtHumid = findViewById(R.id.txtHumidGraph);
        txtHumid.setText(getResources().getString(R.string.humidity) + " (" + sp.getString(Constants.sp_humidity_unit,Constants.sp_default_humid_unit)+")");

        LineSet lineSet = new LineSet();
        for (int i=0;i<weatherArrayList.size();i++) {
            float humid = Float.parseFloat(weatherArrayList.get(i).getHumidity());
            lineSet.addPoint(getXLabel(weatherArrayList.get(i),i),humid);
        }
        lineSet.setSmooth(true);
        lineSet.setColor(getResources().getColor(R.color.humidity_line));
        lineSet.setThickness(5);
        int step =10;
        LineChartView lineChartView = findViewById(R.id.graph_Humid_linechart);
        initLineChar(lineChartView,lineSet,0,100,10);
    }

    @SuppressLint("SetTextI18n")
    private void CreatePressureGraph () {
        TextView txtPressure = findViewById(R.id.txtPressureGraph);
        txtPressure.setText(getResources().getString(R.string.pressure) + " (" + sp.getString(Constants.sp_pressure_unit,Constants.sp_default_pressure_unit)+")");

        float minPressure = UnitConvertor.convertPressure( Float.parseFloat(weatherArrayList.get(0).getPressure()),sp);
        float maxPressure = minPressure;
        LineSet lineSet = new LineSet();
        for (int i=0;i<weatherArrayList.size();i++) {
            float pressure = Float.parseFloat(weatherArrayList.get(i).getPressure());
            minPressure = Math.min(pressure,minPressure);
            maxPressure = Math.max(pressure,maxPressure);
            lineSet.addPoint(getXLabel(weatherArrayList.get(i),i),pressure);
        }
        lineSet.setSmooth(true);
        lineSet.setColor(getResources().getColor(R.color.pressure_line));
        lineSet.setThickness(5);
        int step = Math.max(1,(int)Math.ceil(Math.abs(maxPressure-minPressure)/4));
        LineChartView lineChartView = findViewById(R.id.graph_pressure_linechart);
        initLineChar(lineChartView,lineSet,minPressure,maxPressure,step);
    }

    @SuppressLint("SetTextI18n")
    private void CreateWindGraph() {
        TextView txtWind = findViewById(R.id.txtWindGraph);
        txtWind.setText(getResources().getString(R.string.wind) + " (" + sp.getString(Constants.sp_speed_unit,Constants.sp_default_speed_unit)+")");

        float minWind = (float) UnitConvertor.convertWind( Float.parseFloat(weatherArrayList.get(0).getWind()),sp);
        float maxWind = minWind;
        LineSet lineSet = new LineSet();

        for (int i=0;i<weatherArrayList.size();i++) {
            float wind = Float.parseFloat(weatherArrayList.get(i).getWind());
            minWind = Math.min(wind,minWind);
            maxWind = Math.max(wind,maxWind);
            lineSet.addPoint(getXLabel(weatherArrayList.get(i),i),wind);
        }
        lineSet.setSmooth(true);
        lineSet.setColor(getResources().getColor(R.color.wind_line));
        lineSet.setThickness(5);

        int step = Math.max(1,(int)Math.ceil(Math.abs(maxWind-minWind)/4));

        LineChartView lineChartView = findViewById(R.id.graph_wind_linechart);
        initLineChar(lineChartView,lineSet,minWind,maxWind,step);
    }

    @SuppressLint("SetTextI18n")
    private  void CreateRainGraph() {
            TextView txtRain = findViewById(R.id.txtRainGraph);
            txtRain.setText(getResources().getString(R.string.rain) + " (" + sp.getString(Constants.sp_length_unit, Constants.sp_default_length_unit_mm)+")");

            BarSet barSet = new BarSet();
            float minRain = UnitConvertor.convertRain(Float.parseFloat(weatherArrayList.get(0).getRain()), sp);
            float maxRain = minRain;
            for (int i = 0; i < weatherArrayList.size(); i++) {
                float rain = UnitConvertor.convertRain(Float.parseFloat(weatherArrayList.get(i).getRain()), sp);
                minRain = Math.min(rain, minRain);
                maxRain = Math.max(rain, maxRain);
                Log.d("Min max  ", "min " + minRain + "  max:" + maxRain);
                barSet.addBar(getXLabel(weatherArrayList.get(i), i), rain);
            }
            barSet.setColor(getResources().getColor(R.color.rain_line));
            int step = Math.max(1, (int) Math.ceil(Math.abs(maxRain - minRain) / 4));
            BarChartView barChartView = findViewById(R.id.graph_rain_barchart);
            initLineChar(barChartView, barSet, 0, maxRain, step);
    }

    private String getXLabel (Weather weather,int i) {
        String output = dateFormat.format(weather.getDate()); //lay thu trong tuan
       Calendar calendar = Calendar.getInstance();
        calendar.setTime(weather.getDate());
        int weatherHour = calendar.get(Calendar.HOUR_OF_DAY);

        //phan loai ngay theo gio
        if (i == 0 && weatherHour >13) {
            return output;
        }

        else if (i == weatherArrayList.size() - 1 && weatherHour < 11) {

            return output;
        }
        else if (weatherHour >= 11 && weatherHour <= 13) {
            return output;
        }
        else {
            return "";
        }
    }

    private void init () {
        //set graph toolbar
        Toolbar toolbar = findViewById(R.id.graph_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.graphs);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private ParseResult parseLongtermJson(String result) {
        try {
            Log.d("Json", "parseLongtermJson: "+result);
            JSONObject jsonObject = new JSONObject(result);

            String code = jsonObject.optString(Constants.json_code);
            if (code.equals(Constants.json_code_error)) {
                return ParseResult.CITY_NOT_FOUND;
            }

            JSONArray list = jsonObject.getJSONArray(Constants.json_list);
            for (int i=0;i<list.length();i++) {
                Weather weather = new Weather();

                JSONObject listItem = list.getJSONObject(i);
                JSONObject main = listItem.getJSONObject(Constants.json_main);

                JSONObject winObj= listItem.optJSONObject(Constants.json_wind);
                weather.setWind(winObj.getString(Constants.json_wind_speed));
                weather.setHumidity(main.getString(Constants.json_humidity));
                weather.setPressure(main.getString(Constants.json_pressure));

                JSONObject rainObj = listItem.optJSONObject(Constants.json_rain);
                JSONObject snowObj = listItem.optJSONObject(Constants.json_snow);
                if (rainObj!= null) {
                    weather.setRain(Format.getRainString(rainObj));
                    Log.d("RAIN", "parseLongtermJson: "+weather.getRain());
                }
                else if (snowObj!=null) {
                    weather.setRain(Format.getRainString(snowObj));
                }
               else weather.setRain("0");

                weather.setDate(listItem.getString(Constants.json_date_time));
                weather.setTemperature(main.getString(Constants.json_temperature));

                weatherArrayList.add(weather);
            }
        } catch (JSONException e) {
            Log.e("JSONException Data", result);
            e.printStackTrace();
            return ParseResult.JSON_EXCEPTION;
        }
        return  ParseResult.PARSED;
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent= new Intent(GraphActivity.this,MainActivity.class);
        startActivity(intent);
        return super.onSupportNavigateUp();
    }
}
