package com.example.weatherforecast.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.weatherforecast.Ads.OtherAdsManager;
import com.example.weatherforecast.Ads.UnifiedNativeAdsManager;
import com.example.weatherforecast.Ads.adUtils;
import com.example.weatherforecast.R;
import com.example.weatherforecast.adapter.ViewPagerAdapter;
import com.example.weatherforecast.adapter.WeatherAdapter;
import com.example.weatherforecast.constant.Constants;
import com.example.weatherforecast.fragment.DanhSachFragment;
import com.example.weatherforecast.fragment.FindCityFragment;
import com.example.weatherforecast.model.Weather;
import com.example.weatherforecast.task.ParseResult;
import com.example.weatherforecast.task.TaskOutput;
import com.example.weatherforecast.task.WeatherAsynTask;
import com.example.weatherforecast.utils.Format;
import com.example.weatherforecast.utils.Theme;
import com.example.weatherforecast.utils.UnitConvertor;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.weatherforecast.utils.DayTimeUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static Map<String, Integer> speedUnits = new HashMap<>(3);
    private static Map<String, Integer> pressUnits = new HashMap<>(3);

    private static boolean mappingsInitialised = false;

    AdView adBanner;
    private List<UnifiedNativeAd> unifiedNativeAdList = new ArrayList<>();
    UnifiedNativeAdsManager nativeAdsManager;
    OtherAdsManager otherAdsManager;

    TextView todayTemp,
            todayDescrip,
            todayWind,
            todayPressure,
            todayHumid,
            todaySunrise,
            todaySunset,
            todayIcon,
            lastUpdate,
            coin;

    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    AppBarLayout appBarLayout;
    ProgressDialog progressDialog;

    View viewApp;
    Format format;
    LocationManager locationManager;

    Weather todayWeather = new Weather();
    List<Object> todayList= new ArrayList<>();
    List<Object> tomorrowList= new ArrayList<>();
    List<Object> laterList = new ArrayList<>();

    public String lastestCity = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adBanner = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adBanner.loadAd(adRequest);

        initViewAndConfig();
        initMappings();//hashmap for PressureUnit & speedUnit

        swipeRefreshLayout.setOnRefreshListener(() -> {
            RefreshContent();
            swipeRefreshLayout.setRefreshing(false);
        });

        //allow pull to refresh when scrolling to top
        appBarLayout.addOnOffsetChangedListener((appBarLayout, i) -> swipeRefreshLayout.setEnabled(i == 0));

        preLoadWeather();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onResume() {
        super.onResume();
        //set color
        int theme = Theme.changeColorBg(this);
        getWindow().setStatusBarColor(getResources().getColor(theme));
        appBarLayout.setBackgroundColor(getResources().getColor(theme));
        setSupportActionBar(toolbar);

        if (CheckNetwork()) {
            new TodayWeatherTask(progressDialog,this,this).execute();
            new WeatherListTask(progressDialog,this,this).execute();
        }

        if (nativeAdsManager == null) {
            nativeAdsManager = UnifiedNativeAdsManager.getInstance(getApplication(), 5);
        }

        if (adBanner!=null) adBanner.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adBanner!= null) {
            adBanner.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adBanner!=null) adBanner.destroy();
    }

    @SuppressLint("ResourceAsColor")
    public void initViewAndConfig() {
        todayTemp = findViewById(R.id.todayTemp);
        todayDescrip = findViewById(R.id.todayDescrib);
        todayWind = findViewById(R.id.todayWind);
        todayPressure = findViewById(R.id.todayPressure);
        todayHumid = findViewById(R.id.todayHumidity);
        todaySunrise = findViewById(R.id.todaySunrise);
        todaySunset = findViewById(R.id.todaySunset);
        todayIcon = findViewById(R.id.todayIcon);
        lastUpdate = findViewById(R.id.lastUpdate);
        coin = findViewById(R.id.coins);
        //layout
        swipeRefreshLayout = findViewById(R.id.swipeLayoutRefresh);
        appBarLayout = findViewById(R.id.appBarLayout);
        tabLayout = findViewById(R.id.tabLayout);
        //others
        viewApp = findViewById(R.id.viewApp);
        viewPager = findViewById(R.id.viewPager);
        progressDialog = new ProgressDialog(this);
        format = new Format(this);
        //load Toolbar
        toolbar=findViewById(R.id.toolbar);
        //init ads
        otherAdsManager = OtherAdsManager.getInstance(this);
        nativeAdsManager = UnifiedNativeAdsManager.getInstance(getApplication(),5);
        nativeAdsManager.getUnifiedAdsObservable().observe(this,unifiedNativeAds -> {
            if (unifiedNativeAds.size()>0) {
                unifiedNativeAdList = unifiedNativeAds;
                adUtils.insertAdsInMenuItems(unifiedNativeAdList,laterList);
            }
        } );
    }

    public static void initMappings() {
        if (mappingsInitialised)
            return;
        mappingsInitialised = true;
        speedUnits.put("m/s", R.string.speed_unit_mps);
        speedUnits.put("kph", R.string.speed_unit_kph);
        speedUnits.put("mph", R.string.speed_unit_mph);
        speedUnits.put("kn", R.string.speed_unit_kn);

        pressUnits.put("hPa", R.string.pressure_unit_hpa);
        pressUnits.put("kPa", R.string.pressure_unit_kpa);
        pressUnits.put("mm Hg", R.string.pressure_unit_mmhg);
    }

    public WeatherAdapter getAdapter(int id){
        WeatherAdapter weatherAdapter;
        if (id==0) {
            weatherAdapter = new WeatherAdapter(todayList,this);
        }
        else if (id==1){
            weatherAdapter = new WeatherAdapter(tomorrowList,this);
        }
        else {
            weatherAdapter = new WeatherAdapter(laterList,this);
        }
        return weatherAdapter;
    }

    private void preLoadWeather() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        String lastToday=sp.getString(Constants.sp_lastToday,"");
        if (!lastToday.isEmpty()) {
            new TodayWeatherTask(progressDialog,this,this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,Constants.url_cachedRespond,lastToday);
        }
        String lastLongterm = sp.getString(Constants.sp_lastLongterm,"");
        if (!lastLongterm.isEmpty()) {
            new WeatherListTask(progressDialog,this,this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,Constants.url_cachedRespond,lastLongterm);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        DoWorkInItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    private void DoWorkInItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.MainMenuWeatherMap:
                if (PreferenceManager.getDefaultSharedPreferences(this).getLong(Constants.sp_coin,0)>=30){
                    startActivity(new Intent(MainActivity.this,MapsActivity.class));
                }
                else {
                    Snackbar.make(viewApp,getString(R.string.coin_error),Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.MainMenuGraphs:
                otherAdsManager.showInterstitialAd();
                startActivity(new Intent(MainActivity.this,GraphActivity.class));
                break;
            case R.id.MainMenuLocation:
                findCitiesLocation();
                break;
            case R.id.MainMenuSetting:
                startActivity(new Intent(MainActivity.this,SettingsActivity.class));
                break;
            case R.id.MainMenuAbout:
                Toast.makeText(this,Constants.about_content,Toast.LENGTH_LONG).show();
                break;
            case R.id.SearchMenu:
                SearchForCities();
                break;
            case R.id.RefreshMenu:
                RefreshContent();
                break;
            case R.id.MainMenuEarnCoin:
                otherAdsManager.showRewardAds(this);
                break;
        }
    }

    //Get current Time and put to SharePreferences
    public static long syncCurrentTime(SharedPreferences sharedPreferences){
        Calendar present = Calendar.getInstance();
        sharedPreferences.edit().putLong(Constants.sp_lastUpdate, present.getTimeInMillis()).commit();
        return present.getTimeInMillis();
    }

    //Check if network is available
    private boolean CheckNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo!=null  &&  networkInfo.isConnectedOrConnecting();
    }

    //Freshing weather list and today List
    public void RefreshContent() {
        if (CheckNetwork()) {
            new TodayWeatherTask(progressDialog,this,this).execute();
            new WeatherListTask(progressDialog,this,this).execute();
        }
        else {
            Snackbar.make(viewApp,getString(R.string.msg_connection_not_available),Snackbar.LENGTH_LONG).show();
        }
    }

    private String localize(SharedPreferences sp, String preferenceKey, String defaultValueKey) {
        return localize(sp, this, preferenceKey, defaultValueKey);
    }

    //interact with hashpmap SpeepUnit & pressureUnit
    public static String localize(SharedPreferences sp, Context context, String preferenceKey, String defaultValueKey) {
        String preferenceValue = sp.getString(preferenceKey, defaultValueKey);
        String result = preferenceValue;
        if (Constants.sp_speed_unit.equals(preferenceKey)) {
            if (speedUnits.containsKey(preferenceValue)) {
                result = context.getString(speedUnits.get(preferenceValue));
            }
        } else if (Constants.sp_pressure_unit.equals(preferenceKey)) {
            if (pressUnits.containsKey(preferenceValue)) {
                result = context.getString(pressUnits.get(preferenceValue));
            }
        }
        return result;
    }

    private void locationAlert () {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.location_settings);
        builder.setMessage(R.string.location_settings_message);
        builder.setPositiveButton(R.string.location_settings_button, (dialog, which) -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)));
        builder.setNegativeButton(R.string.dialog_cancel, (dialog, which) -> dialog.cancel());
        builder.show();
    }

    //Use when Update self-location
    private void findCitiesLocation ( ){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
                //show location permission alert
               locationAlert();
            }
            else {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
        }
        else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //tao progress doi load
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.getting_location));
            progressDialog.setCancelable(false);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.dialog_cancel), (dialog, which) -> {
                try {// click cancel thi huy
                    locationManager.removeUpdates(MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            progressDialog.show();
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);
            }
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
            }
        }
        else {
            locationAlert();
        }
    }

    //Show Search Dialog when Click Search Icon
    private void SearchForCities() {
        final TextInputEditText textInputEditText = new TextInputEditText(this);
        textInputEditText.setHint(R.string.hint_city);
        textInputEditText.setSingleLine(true);
        TextInputLayout textInputLayout = new TextInputLayout(this);
        textInputLayout.setPadding(20,10,20,0);
        textInputLayout.addView(textInputEditText);

        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle(R.string.search_title);
        builder.setView(textInputLayout);
        builder.setPositiveButton(R.string.dialog_ok, (dialog, which) -> new FindCitiesTask(progressDialog,getApplicationContext(),MainActivity.this).execute("city",textInputEditText.getText().toString()));
        builder.setNegativeButton(R.string.dialog_cancel, (dialog, which) -> {

        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onLocationChanged(Location location) {
        progressDialog.hide();
        locationManager.removeUpdates(this);
        String lat= Double.toString(location.getLatitude());
        String lon= Double.toString(location.getLongitude());
        new findCityCoordsTask(progressDialog,this,this).execute(Constants.url_findByCoords_API,lat,lon);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    class TodayWeatherTask extends WeatherAsynTask {

        public TodayWeatherTask(ProgressDialog progressDialog, Context context, MainActivity activity) {
            super(progressDialog, context, activity);
        }

        @Override
        protected void onPreExecute() {
            loading = 0;
            super.onPreExecute();
        }

        @Override
        protected String getAPIname() {
            return Constants.url_currentWeather_API;
        }

        @Override
        protected ParseResult ParseRespond(String respond) {
            return parseTodayJson(respond);
        }

        @Override
        protected void UpdateUI() {
            updateTodayWeatherUI();
        }
    }

    //Parse JSON for ToDay Weather
    private ParseResult parseTodayJson (String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String code = jsonObject.optString(Constants.json_code);
            if (code.equals(Constants.json_code_error)) {
                return ParseResult.CITY_NOT_FOUND;
            }
            //get city, country, sunrise, sunset
            String city = jsonObject.getString(Constants.json_city_name);
            Log.d("CITY TODAY", "parseTodayJson: "+city);
            String country ="";
            JSONObject countryObj= jsonObject.optJSONObject(Constants.json_country);
            if (countryObj!= null) {
                country = countryObj.getString(Constants.json_country_name);
                todayWeather.setSunset(countryObj.getString(Constants.json_country_sunset));
                todayWeather.setSunrise(countryObj.getString(Constants.json_country_sunrise));
            }
            todayWeather.setCountry(country);
            todayWeather.setCity(city);

            //get lat & lon
            JSONObject coordinates = jsonObject.getJSONObject(Constants.json_coodinate);
            if (coordinates!= null) {
                todayWeather.setLat(coordinates.getDouble(Constants.json_coodinate_latitude));
                todayWeather.setLon(coordinates.getDouble(Constants.json_coodinate_lonitude));
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                preferences.edit().putFloat(Constants.sp_latitude,(float) todayWeather.getLat()).putFloat(Constants.sp_longitude,(float)todayWeather.getLon()).commit();
            }

            JSONObject main = jsonObject.getJSONObject(Constants.json_main);
            //get temp and descrip
            todayWeather.setTemperature(main.getString(Constants.json_temperature));
            todayWeather.setDescribtion(jsonObject.getJSONArray(Constants.json_weather).getJSONObject(0).getString(Constants.json_description));

            //wind
            JSONObject windObj = jsonObject.getJSONObject(Constants.json_wind);
            todayWeather.setWind(windObj.getString(Constants.json_wind_speed));
            if (windObj.has(Constants.json_wind_degree)) {
                todayWeather.setWindDirectionDec(windObj.getDouble(Constants.json_wind_degree));
            }
            else {
                todayWeather.setWindDirectionDec(null);
            }

            //presure,humidity
            todayWeather.setPressure(main.getString(Constants.json_pressure));
            todayWeather.setHumidity(main.getString(Constants.json_humidity));

            //rain
            JSONObject rainObj = jsonObject.optJSONObject(Constants.json_rain);
            String rain;
            if (rainObj !=null) {
                rain = Format.getRainString(rainObj);
            }
            else {
                JSONObject snowObj = jsonObject.optJSONObject(Constants.json_snow);
                if (snowObj != null) {
                    rain= Format.getRainString(snowObj);
                }
                else {
                    rain =Constants.json_default_rain_value;
                }
            }
            todayWeather.setRain(rain);

            String idString = jsonObject.getJSONArray(Constants.json_weather).getJSONObject(0).getString(Constants.json_weather_id);
            todayWeather.setId(idString);
            todayWeather.setIcon(format.setIcon(Integer.parseInt(idString),DayTimeUtils.isDayTime(todayWeather,Calendar.getInstance())));

            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
            //save data json TodayWeather gan nhat -->l
            editor.putString(Constants.sp_lastToday,result).commit();

        } catch (JSONException e) {
            e.printStackTrace();
            return ParseResult.JSON_EXCEPTION;
        }

        return ParseResult.PARSED;
    }

    @SuppressLint("SetTextI18n")
    private void updateTodayWeatherUI() {
        try {
            if (todayWeather.getCountry().isEmpty()) {
                preLoadWeather();
                return;
            }
        } catch (Exception e) {
            preLoadWeather();
            return;
        }
        String city = todayWeather.getCity();
        String country = todayWeather.getCountry();
        DateFormat dateFormat= android.text.format.DateFormat.getTimeFormat(getApplicationContext());
        getSupportActionBar().setTitle(city+ (country.isEmpty()? "":","+country));

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        //temp
        Float temp= UnitConvertor.convertTemp(Float.parseFloat(todayWeather.getTemperature()),sharedPreferences);
        if (sharedPreferences.getBoolean(Constants.sp_tempInteger,false)) {
            temp =(float) Math.round(temp);
        }

        //rain
        double rain = Double.parseDouble(todayWeather.getRain());
        String rainString= UnitConvertor.getRainString(rain,sharedPreferences);

        //Wind
        double wind;
        try {
            wind = Double.parseDouble(todayWeather.getWind());
        } catch (NumberFormatException e) {
            wind=0;
        }
        wind = UnitConvertor.convertWind(wind,sharedPreferences);

        //pressure
        double pressure = UnitConvertor.convertPressure(Float.parseFloat(todayWeather.getPressure()),sharedPreferences);

        //set text for Today View
        todayTemp.setText(new DecimalFormat(Constants.sp_decimal_format0x).format(temp)+" "+sharedPreferences.getString(Constants.sp_unit,Constants.sp_default_unit));

        todayDescrip.setText(todayWeather.getDescribtion().substring(0,1).toUpperCase()
                + todayWeather.getDescribtion().substring(1)+rainString);

        if (sharedPreferences.getString(Constants.sp_speed_unit,Constants.sp_default_speed_unit).equals(Constants.sp_speed_unit_bft)) {
            todayWind.setText(getString(R.string.wind) +": "+ new DecimalFormat(Constants.sp_defalt_decimal_format00).format(wind)
                    + " " + UnitConvertor.getBeaufortName((int)wind,this)
                    + (todayWeather.isWindDirectionAvailable()?" "+Format.getWindDirectionString(sharedPreferences,this,todayWeather):""));
        }
        else {
            todayWind.setText(getString(R.string.wind) +": "+ new DecimalFormat(Constants.sp_defalt_decimal_format00).format(wind)
                    + " " + localize(sharedPreferences,Constants.sp_speed_unit,Constants.sp_default_speed_unit)
                    + (todayWeather.isWindDirectionAvailable()?" "+Format.getWindDirectionString(sharedPreferences,this,todayWeather):""));
        }

        todayPressure.setText(getString(R.string.pressure)+ ": "+ new DecimalFormat(Constants.sp_defalt_decimal_format00).format(pressure)+" "
                +localize(sharedPreferences,Constants.sp_pressure_unit,Constants.sp_default_pressure_unit));

        todayHumid.setText(getString(R.string.humidity)+": "+todayWeather.getHumidity()+Constants.sp_default_humid_unit);
        todaySunrise.setText(getString(R.string.sunrise)+": "+dateFormat.format(todayWeather.getSunrise()));
        todaySunset.setText(getString(R.string.sunset)+": "+dateFormat.format(todayWeather.getSunset()));
        todayIcon.setText(todayWeather.getIcon());

        //lastUpdate
        long time = sharedPreferences.getLong(Constants.sp_lastUpdate,0);
        Date lastUpdateDate= new Date(time);
        String formatedTime = getString(R.string.last_update, android.text.format.DateFormat.getTimeFormat(this).format(lastUpdateDate));
        lastUpdate.setText(formatedTime);

        //coin
        String formatCoin = getString(R.string.coin,String.valueOf(sharedPreferences.getLong(Constants.sp_coin,0)));
        coin.setText(formatCoin);
    }

    //do when Initialize data for Graph
    class findCityCoordsTask extends WeatherAsynTask {

        public findCityCoordsTask(ProgressDialog progressDialog, Context context, MainActivity activity) {
            super(progressDialog, context, activity);
        }

        @Override
        protected void onPreExecute() { }


        @Override
        protected String getAPIname() {
            return Constants.url_currentWeather_API;
        }

        @Override
        protected void onPostExecute(TaskOutput taskOutput) {
            handleTaskOutput(taskOutput);
            RefreshContent();
        }

        @Override
        protected ParseResult ParseRespond(String respond) {
            try {
                JSONObject jsonObject = new JSONObject(respond);
                Log.d("jsonobj", "ParseRespond: "+jsonObject.getString(Constants.json_weather_id));
                String code = jsonObject.getString(Constants.json_code);
                if (code.equals(Constants.json_code_error)){
                    return ParseResult.CITY_NOT_FOUND;
                }
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                lastestCity = sp.getString(Constants.sp_cityId,Constants.DEFAULT_CITY_ID);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString(Constants.sp_cityId,jsonObject.getString(Constants.json_weather_id));
                editor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
                return ParseResult.JSON_EXCEPTION;
            }
            return ParseResult.PARSED;
        }
    }

    //Do when load Internet data to get ListWeather
    class WeatherListTask extends WeatherAsynTask {

        public WeatherListTask(ProgressDialog progressDialog, Context context, MainActivity activity) {
            super(progressDialog, context, activity);
        }
        @Override
        protected String getAPIname() {
            return Constants.url_hourlyForecast_API;
        }

        @Override
        protected ParseResult ParseRespond(String respond) {
            return parseListJson(respond) ;
        }

        @Override
        protected void UpdateUI() {
            updateWeatherList();
        }

        private void updateWeatherList(){
            //loadNativeAds();

            ViewPagerAdapter viewPagerAdapter= new ViewPagerAdapter(getSupportFragmentManager());
            //set ToDay List
            Bundle todayBundle= new Bundle();
            todayBundle.putInt(Constants.day,Constants.TODAY_ID);
            DanhSachFragment todayDanhSachFragment = new DanhSachFragment();
            todayDanhSachFragment.setArguments(todayBundle);
            viewPagerAdapter.addNewFragment(todayDanhSachFragment,Constants.today);
            //Tomorrow List
            Bundle tomorrowBundle= new Bundle();
            tomorrowBundle.putInt(Constants.day,Constants.TOMORROW_ID);
            DanhSachFragment tomorrowDanhSachFragment = new DanhSachFragment();
            tomorrowDanhSachFragment.setArguments(tomorrowBundle);
            viewPagerAdapter.addNewFragment(tomorrowDanhSachFragment,Constants.tomorrow);
            //Later List
            Bundle laterBundle= new Bundle();
            laterBundle.putInt(Constants.day,Constants.LATER_ID);
            DanhSachFragment laterDanhSachFragment = new DanhSachFragment();
            laterDanhSachFragment.setArguments(laterBundle);
            viewPagerAdapter.addNewFragment(laterDanhSachFragment,Constants.later);

            int currentPage= viewPager.getCurrentItem();
            //set up viewPager
            viewPagerAdapter.notifyDataSetChanged();
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.setCurrentItem(currentPage,false);
        }

        @Override
        protected void onPostExecute(TaskOutput taskOutput) {
            if (CheckNetwork()) {
                adUtils.insertAdsInMenuItems(unifiedNativeAdList,laterList);
            }
            UpdateUI();
        }
    }

    //Parse Json for WeatherListTask
    private ParseResult parseListJson (String result)  {
        try {
            JSONObject listObj= new JSONObject(result);

            String code= listObj.optString(Constants.json_code);
            if (code.equals(Constants.json_code_error)) {
                if (todayList== null) {
                    todayList = new ArrayList<>();
                    tomorrowList = new ArrayList<>();
                    laterList = new ArrayList<>();
                }
                return ParseResult.CITY_NOT_FOUND;
            }

            todayList = new ArrayList<>();
            tomorrowList = new ArrayList<>();
            laterList = new ArrayList<>();
            JSONArray list = listObj.getJSONArray(Constants.json_list);

            for (int i=0;i<list.length();i++) {
                Weather itemWeather = new Weather();

                JSONObject listItem = list.getJSONObject(i);
                JSONObject main = listItem.getJSONObject(Constants.json_main);
                JSONArray weather = listItem.getJSONArray(Constants.json_weather);

                itemWeather.setDate(listItem.getString(Constants.json_date_time));
                itemWeather.setTemperature(main.getString(Constants.json_temperature));
                itemWeather.setPressure(main.getString(Constants.json_pressure));
                itemWeather.setHumidity(main.getString(Constants.json_humidity));
                itemWeather.setDescribtion(weather.getJSONObject(0).getString(Constants.json_description));
                itemWeather.setId(weather.getJSONObject(0).getString(Constants.json_weather_id));

                JSONObject windObj = listItem.getJSONObject(Constants.json_wind);
                if (windObj!= null) {
                    itemWeather.setWind(windObj.getString(Constants.json_wind_speed));
                    itemWeather.setWindDirectionDec(windObj.getDouble(Constants.json_wind_degree));
                }

                JSONObject rainObj = listItem.optJSONObject(Constants.json_rain);
                String rain="";
                if (rainObj != null) {
                    rain = Format.getRainString(rainObj);
                }
                else {
                    JSONObject snowObj= listItem.optJSONObject(Constants.json_snow);
                    if (snowObj!= null) {
                        rain= Format.getRainString(snowObj);
                    }
                    else {
                        rain=Constants.json_default_rain_value;
                    }
                }
                itemWeather.setRain(rain);

                Calendar calendar = Calendar.getInstance();
                String dateString = listItem.getString(Constants.json_date_time)+"000";
                calendar.setTimeInMillis(Long.parseLong(dateString));
                itemWeather.setIcon(format.setIcon(Integer.parseInt(itemWeather.getId()),DayTimeUtils.isDayTime(itemWeather,calendar)));

                //set time
                Calendar today = Calendar.getInstance();
                today.set(Calendar.HOUR_OF_DAY,0);
                today.set(Calendar.MINUTE,0);
                today.set(Calendar.SECOND,0);
                today.set(Calendar.MILLISECOND,0);

                Calendar tomorrow = (Calendar) today.clone();
                tomorrow.add(Calendar.DAY_OF_YEAR,1);

                Calendar later = (Calendar) today.clone();
                later.add(Calendar.DAY_OF_YEAR,2);

                if (calendar.before(tomorrow)) {
                    todayList.add(itemWeather);
                }
                else if (calendar.before(later)) {
                    tomorrowList.add(itemWeather);
                }
                else {
                    laterList.add(itemWeather);
                }
            }
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putString(Constants.sp_lastLongterm,result).commit();
        }
        catch (JSONException e) {
            e.printStackTrace();
            return ParseResult.JSON_EXCEPTION;
        }
        return ParseResult.PARSED;
    }

    //Do when User find cities
    class FindCitiesTask extends WeatherAsynTask {


        public FindCitiesTask(ProgressDialog progressDialog, Context context, MainActivity activity) {
            super(progressDialog, context, activity);
        }

        @Override
        protected String getAPIname() {
            return Constants.url_cityFinding_API;
        }

        @Override
        protected ParseResult ParseRespond(String respond) {
            return parseCitiesJson(respond);
        }

        @Override
        protected void UpdateUI() {
            RefreshContent();
        }
    }

    //Parse City for FindCities Task
    private ParseResult parseCitiesJson (String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);

            String code = jsonObject.optString(Constants.json_code);
            if (code.equals(Constants.json_code_error)) {
                return ParseResult.CITY_NOT_FOUND;
            }

            JSONArray cityArray = jsonObject.getJSONArray(Constants.json_list);
            if (cityArray.length()>1) {
                showLocationFragment(cityArray);
            }
            else {
                SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(this);
                sp.edit().putString(Constants.sp_cityId,cityArray.getJSONObject(0).getString(Constants.json_weather_id)).commit();
                lastestCity = sp.getString(Constants.sp_cityId,Constants.DEFAULT_CITY_ID);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return ParseResult.JSON_EXCEPTION;
        }
        return ParseResult.PARSED;
    }

    //Show Fragment when Finding many cities
    private void  showLocationFragment (JSONArray arrayCity)  {
        FindCityFragment findCityFragment = new FindCityFragment();
        Bundle bundle = new Bundle();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        bundle.putString(Constants.bundle_city_list, arrayCity.toString());
        findCityFragment.setArguments(bundle);

        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.add(android.R.id.content,findCityFragment).addToBackStack(null).commit();

    }

}
