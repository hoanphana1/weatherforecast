package com.example.weatherforecast.constant;

public class Constants {
    public static final String day = "day";
    public static final String today= "Today";
    public static final int TODAY_ID=0;
    public static final int TOMORROW_ID=1;
    public static final int LATER_ID=2;
    public static final String tomorrow = "Tomorrow";
    public static final String later = "Later";

//------------------Map Constants------------------------------
    public static final float  map_zoomlevel = 8f;


    //-----------default city id----------------
    public static final String DEFAULT_CITY_ID = "1580240";//Hue,VN

    //--------------Bundle Const------------------------
    public static String bundle_should_refresh = "shouldRefresh";
    public static String bundle_city_list = "cityList";

    //----------URL CONSTANTS--------------
    public static final String url_currentWeather_API="weather";
    public static final String url_hourlyForecast_API="forecast";
    public static final String url_cityFinding_API="find";
    public static final String url_findByCoords_API="coords";
    public static final String url_findByCity_API="city";
    public static final String url_cachedRespond = "cachedResponse";

    //-----------JSON CONSTANTS----------------
     public static final String json_code= "cod";
    public static final String json_code_error= "404";

    public static final String json_city_name= "name";
    public static final String json_country = "sys";
    public static final String json_country_name = "country";
    public static final String json_country_sunset = "sunset";
    public static final String json_country_sunrise= "sunrise";

    public static final String json_coodinate = "coord";
    public static final String json_coodinate_latitude="lat";
    public static final String json_coodinate_lonitude="lon";

    public static final String json_list= "list";
    public static final String json_main= "main";
    public static final String json_weather="weather";
    public static final String json_date_time= "dt";
    public static final String json_temperature= "temp";
    public static final String json_pressure="pressure";
    public static final String json_humidity= "humidity";
    public static final String json_description= "description";
    public static final String json_weather_id="id";
    public static final String json_wind = "wind";
    public static final String json_wind_speed ="speed";
    public static final String json_wind_degree= "deg";

    public static final String json_rain= "rain";
    public static final String json_default_rain_value= "0";
    public static final String json_rain_3h = "3h";
    public static final String json_rain_1h = "1h";
    public static final String json_rain_fail = "fail";
    public static final String json_snow="snow";


    //-------------SharedPreferences Constants----------------
    public static final String sp_theme = "theme";
    public static final String sp_theme_default ="default";
    public static final String sp_theme_morning_glory ="morningGlory";
    public static final String sp_theme_half_baked = "halfBaked";
    public static final String sp_theme_peach_yellow="peachYellow";
    public static final String sp_theme_Cherokee= "cherokee";
    public static final String sp_theme_feijoa="feijoa";

    public static final String sp_cityId = "cityId";

    public static final String sp_coin ="coins";

    public static final String sp_lastToday= "lastToday";
    public static final String sp_lastLongterm="lastLongterm";
    public static final String sp_lastUpdate ="lastUpdate";

    public static final String sp_unit="unit";
    public static final String sp_default_unit="°C";
    public static final String sp_unit_F="°F";
    public static final String sp_tempInteger="tempInteger";

    public static final String sp_speed_unit="SpeedUnit";
    public static final String sp_default_speed_unit="m/s";
    public static final String sp_speed_unit_bft="bft";
    public static final String sp_speed_unit_kph="kph";
    public static final String sp_speed_unit_mph="mph";
    public static final String sp_speed_unit_kn ="kn";

    public static final String sp_pressure_unit="PressureUnit";
    public static final String sp_default_pressure_unit="hPa";
    public static final String sp_pressure_unit_kpa ="kPa";
    public static final String sp_pressure_unit_mmhg = "mm Hg";
    public static final String sp_pressure_unit_inhg = "in Hg";

    public static final String sp_decimal_display= "displayDecimalZeroes";
    public static final String sp_defalt_decimal_format00="0.0";
    public static final String sp_decimal_format0x="0.#";

    public static final String sp_humidity_unit="HumidityUnit";
    public static final String sp_default_humid_unit="%";

    public static final String sp_length_unit="LengthUnit";
    public static final String sp_default_length_unit_mm="mm";

    public static final String sp_latitude ="latitude";
    public static final String sp_longitude = "longitude";

    public static final String sp_windDirection_format ="windDirectionFormat";
    public static final String sp_default_wind_Format="arrow";
    public static final String sp_wind_format_abbr="abbr";

    public static final String about_content = "Dang Minh Nhat \n " +
                                                "Foxcode Internship Program \n "+
                                                "Weather Forecast App";

}
