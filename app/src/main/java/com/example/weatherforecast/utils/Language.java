package com.example.weatherforecast.utils;

import java.util.Locale;

public class Language {
    public static String getLanguage(){
        String language = Locale.getDefault().getLanguage();

        if (language.equals(new Locale("cs").getLanguage())) {
            //Czech
            return "cz";
        }
        else if (language.equals(new Locale("lv").getLanguage())) {
            return "la";
        }
        else if (language.equals(new Locale("ko").getLanguage())) {
            return "kr";
        }
        return language;
    }
}
