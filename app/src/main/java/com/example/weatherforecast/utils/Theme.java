package com.example.weatherforecast.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.Window;

import androidx.preference.PreferenceManager;

import com.example.weatherforecast.R;
import com.example.weatherforecast.constant.Constants;

public class Theme {
    public static int changeColorBg(Context context) {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        Log.d("color", "changeColorBg: "+sp.getString(Constants.sp_theme,Constants.sp_theme_default));
       switch(sp.getString(Constants.sp_theme,Constants.sp_theme_default)) {
           case Constants.sp_theme_Cherokee:
               return R.color.cherokee;
           case Constants.sp_theme_feijoa:
               return R.color.feijoa;
           case Constants.sp_theme_half_baked:
               return R.color.half_baked;
           case Constants.sp_theme_morning_glory:
               return R.color.morning_glory;
           case Constants.sp_theme_peach_yellow:
               return R.color.peach_yellow;
           default:  return R.color.lightBlue;
        }
    }
}
