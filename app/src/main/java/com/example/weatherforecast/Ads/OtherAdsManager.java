package com.example.weatherforecast.Ads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Icon;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import com.example.weatherforecast.R;
import com.example.weatherforecast.constant.Constants;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

public class OtherAdsManager {
    private static OtherAdsManager instance;
    private Context context;

    private InterstitialAd interstitialAd;
    private RewardedAd rewardedAd;

    private int mLoadAdsTime;
    private int mLoadVideoTime;

    private final int COUNT_DOWN = 10;
    private String K_SHOW_ADS = "show_ads";


    private OtherAdsManager(Context context) {
        this.context = context;

        mLoadAdsTime=0;
        interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(context.getString(R.string.interstitial_ad_unit_id));
        setInterstitialAdListener();
        loadAdInterstitialAd();

        mLoadVideoTime = 0;
        rewardedAd = new RewardedAd(context,context.getString(R.string.reward_ad_unit_id));
        loadRewardAd();


    }

    public static synchronized  OtherAdsManager getInstance(Context context) {
        if (instance ==null) {
            instance = new OtherAdsManager(context);
        }
        return instance;
    }

    public void loadAdInterstitialAd () {
        if (interstitialAd!= null && mLoadAdsTime<5){
            interstitialAd.loadAd(new AdRequest.Builder().build());
        }

    }

    public void setInterstitialAdListener () {
        interstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                loadAdInterstitialAd();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                loadAdInterstitialAd();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                mLoadAdsTime =0;
                loadAdInterstitialAd();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }
        });
    }

    public void showInterstitialAd() {
        if (canShowAds()) {
            if (interstitialAd.isLoaded()){
                SharedPreferences sharedPreferences = context.getSharedPreferences(K_SHOW_ADS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong(K_SHOW_ADS, System.currentTimeMillis()).commit();

                interstitialAd.show();
            }
            else {
                loadAdInterstitialAd();
            }
        }
        else {
            Log.d("interstitial ads", "not loaded ");
        }
    }

    RewardedAdLoadCallback rewardedAdLoadCallback = new RewardedAdLoadCallback() {
        @Override
        public void onRewardedAdFailedToLoad(int i) {
            Toast.makeText(context, "no Ads available!", Toast.LENGTH_SHORT).show();
            super.onRewardedAdFailedToLoad(i);
            loadRewardAd();
        }
    };

    RewardedAdCallback rewardedAdCallback = new RewardedAdCallback() {
        @Override
        public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
            mLoadVideoTime =0;
            loadRewardAd();
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            long coin = sp.getLong(Constants.sp_coin,0);
            coin+=10;
            sp.edit().putLong(Constants.sp_coin,coin).commit();
        }
        @Override
        public void onRewardedAdClosed() {
            loadRewardAd();
            super.onRewardedAdClosed();
        }

        @Override
        public void onRewardedAdFailedToShow(int i) {
            loadRewardAd();
            super.onRewardedAdFailedToShow(i);
        }

    };

    public void loadRewardAd() {
        if (rewardedAd!= null && mLoadVideoTime<5) {
            rewardedAd = new RewardedAd(context, context.getString(R.string.reward_ad_unit_id));
            rewardedAd.loadAd( new AdRequest.Builder().build(),rewardedAdLoadCallback);
            mLoadVideoTime++;
        }
    }

    public void showRewardAds (Activity activity) {
        mLoadVideoTime =0;
        if (canShowAds()) {
            if (rewardedAd!= null) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(K_SHOW_ADS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong(K_SHOW_ADS, System.currentTimeMillis());
                editor.commit();
                rewardedAd.show( activity,rewardedAdCallback );
            }
        }
        else {
            Log.d("ADSS", "showRewardAds: cant show");
        }
    }

    public boolean canShowAds() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(K_SHOW_ADS, Context.MODE_PRIVATE);
        long showAds = sharedPreferences.getLong(K_SHOW_ADS, 0);
        return (System.currentTimeMillis() - showAds > COUNT_DOWN);
    }
}
