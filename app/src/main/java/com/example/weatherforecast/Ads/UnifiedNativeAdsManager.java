package com.example.weatherforecast.Ads;

import android.app.Application;
import android.database.Observable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.weatherforecast.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UnifiedNativeAdsManager {
    private static final String LOG_TAG = "UnifiedNativeAdsManager";
    private static final int MAX_RETRY_COUNT = 2;
    private static final int DELAY_FACTOR = 2;

    private final List<UnifiedNativeAd> ads;
    private MutableLiveData<List<UnifiedNativeAd>> adsSubject = new MutableLiveData<>();
//    private final BehaviorSubject<List<UnifiedNativeAd>> adsSubject =
//            BehaviorSubject.createDefault(Collections.<UnifiedNativeAd>emptyList());


    private final int numberOfAds;
    private final AdLoader adLoader;

    private final Handler mainHandler = new Handler (Looper.getMainLooper());
    private int loadCount = 0;
    private int currentDelay = 1_000; // milliseconds;

    private static volatile UnifiedNativeAdsManager instance;

    public static UnifiedNativeAdsManager getInstance(@NonNull Application context, int numberOfAds) {
        UnifiedNativeAdsManager temp = instance;
        if (temp == null) {
            synchronized (UnifiedNativeAdsManager.class) {
                temp = instance;
                if (temp == null) {
                    instance = temp = new UnifiedNativeAdsManager(context, numberOfAds);
                }
            }
        }
        return temp;
    }

    private UnifiedNativeAdsManager(@NonNull Application context, int numberOfAds) {
        this.numberOfAds = numberOfAds;
        this.ads = new ArrayList<>(numberOfAds);

        VideoOptions videoOptions = new VideoOptions.Builder().setStartMuted(true).build();
        NativeAdOptions nativeAdOptions = new NativeAdOptions.Builder().setVideoOptions(videoOptions).build();

        adLoader = new AdLoader.Builder(context, context.getResources().getString(R.string.native_ad_unit_id))
                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        Log.d(LOG_TAG, "onUnifiedNativeAdLoaded unifiedNativeAd=" + unifiedNativeAd + ",adLoader.isLoading()=" + adLoader.isLoading());

                        if (adLoader.isLoading()) {
                            ads.add(unifiedNativeAd);
                        } else {
                            adsSubject.setValue(ads);
                        }
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        Log.d(LOG_TAG, "onAdFailedToLoad errorCode=" + errorCode + ",loadCount=" + loadCount);

                        if (loadCount <= MAX_RETRY_COUNT) {
                            currentDelay = currentDelay * DELAY_FACTOR;
                            mainHandler.postDelayed(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            loadAds();
                                        }
                                    },
                                    currentDelay
                            );
                            Log.d(LOG_TAG, "Schedule loadAds after " + currentDelay + " ms");
                        }
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        loadAds();
    }

    private void loadAds() {
        loadCount++;
        adLoader.loadAds(new AdRequest.Builder().build(), numberOfAds);
        Log.d(LOG_TAG, "Call loadAds loadCount=" + loadCount);
    }

    public MutableLiveData<List<UnifiedNativeAd>> getUnifiedAdsObservable() {
        return adsSubject;
    }

}


