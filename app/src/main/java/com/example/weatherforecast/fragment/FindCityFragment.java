package com.example.weatherforecast.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.R;
import com.example.weatherforecast.activity.MainActivity;
import com.example.weatherforecast.adapter.FindCityAdapter;
import com.example.weatherforecast.constant.Constants;
import com.example.weatherforecast.model.Weather;
import com.example.weatherforecast.utils.DayTimeUtils;
import com.example.weatherforecast.utils.Format;
import com.example.weatherforecast.utils.UnitConvertor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class FindCityFragment extends DialogFragment implements FindCityAdapter.ItemClickListener {

    FindCityAdapter adapter;
    SharedPreferences sp;
    View.OnClickListener onClickListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_city_fragment,container,false);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Bundle bundle = getArguments();
        Toolbar toolbar = view.findViewById(R.id.findCityToolbar);
        Format format = new Format(getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.findCityRecycleView);

        toolbar.setTitle("Location");
        toolbar.setNavigationOnClickListener(onClickListener);

        ParseJson(bundle,format);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

    }

    private void init() {
        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        };
    }

    private void ParseJson (Bundle bundle,Format format) {
        try {
            JSONArray cityArray= new JSONArray(bundle.getString("cityList"));
            ArrayList<Weather> weatherArrayList = new ArrayList<>();
            adapter = new FindCityAdapter(getActivity().getApplicationContext(),weatherArrayList,this);
            for (int i=0;i<cityArray.length();i++) {
                JSONObject cityObj = cityArray.getJSONObject(i);
                JSONObject weatherObj = cityObj.getJSONArray(Constants.json_weather).getJSONObject(0);
                JSONObject mainObj = cityObj.getJSONObject(Constants.json_main);
                JSONObject coordObj = cityObj.getJSONObject(Constants.json_coodinate);
                JSONObject sysObj = cityObj.getJSONObject(Constants.json_country);

                Calendar calendar = Calendar.getInstance();
                String date = cityObj.getString(Constants.json_date_time);
                String city = cityObj.getString(Constants.json_city_name);
                String country = sysObj.getString(Constants.json_country_name);
                String cityId= cityObj.getString(Constants.json_weather_id);
                String description = weatherObj.getString(Constants.json_description);
                String weatherId = weatherObj.getString(Constants.json_weather_id);
                float temp = UnitConvertor.convertTemp(Float.parseFloat(mainObj.getString(Constants.json_temperature)),sp);
                double lat = coordObj.getDouble(Constants.json_coodinate_latitude);
                double lon = coordObj.getDouble(Constants.json_coodinate_lonitude);

                calendar.setTimeInMillis(Long.parseLong(date));
                Weather weather = new Weather();
                weather.setCity(city);
                weather.setCountry(country);
                weather.setId(cityId);
                weather.setDescribtion(description.substring(0,1).toUpperCase()+description.substring(1));
                weather.setLat(lat);
                weather.setLon(lon);
                weather.setIcon(format.setIcon(Integer.parseInt(weatherId), DayTimeUtils.isDayTime(weather,calendar)));

                if (sp.getBoolean(Constants.sp_decimal_display,false)) {
                    weather.setTemperature(new DecimalFormat(Constants.sp_defalt_decimal_format00).format(temp)+" "+sp.getString(Constants.sp_unit,Constants.sp_default_unit));
                }
                else {
                    weather.setTemperature(new DecimalFormat(Constants.sp_decimal_format0x).format(temp)+" "+sp.getString(Constants.sp_unit,Constants.sp_default_unit));
                }

                weatherArrayList.add(weather);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnItemClickListener(View view, int position) {
        Weather weather = adapter.getItem(position);
        Intent intent= new Intent(getActivity(), MainActivity.class);
        Bundle bundle = new Bundle();

        sp.edit().putString(Constants.sp_cityId,weather.getId()).commit();
        bundle.putBoolean(Constants.bundle_should_refresh,true);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
