package com.example.weatherforecast.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.R;
import com.example.weatherforecast.activity.MainActivity;
import com.example.weatherforecast.constant.Constants;
import com.example.weatherforecast.model.Weather;
import com.example.weatherforecast.utils.Format;
import com.example.weatherforecast.utils.UnitConvertor;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import org.w3c.dom.Text;

import java.lang.annotation.Native;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class WeatherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Object> weatherList;
    Context context;

    private static final int ITEM_VIEW_TYPE = 0;
    private static final int NATIVE_ADS_VIEW_TYPE =1;

    public WeatherAdapter(List<Object> weatherList, Context context) {
        this.weatherList = weatherList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(context);
        switch (viewType) {
            case NATIVE_ADS_VIEW_TYPE:
                View NativeAdsView = inflater.inflate(R.layout.ad_native_united,parent,false);
                return new NativeAdsViewHolder(NativeAdsView);
            case ITEM_VIEW_TYPE:
            default:
                View ItemView=inflater.inflate(R.layout.weather_row,parent,false);
                return new ItemViewHolder(ItemView);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case NATIVE_ADS_VIEW_TYPE:
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) weatherList.get(position);
                parseNativeAdView(nativeAd,((NativeAdsViewHolder)holder).getNativeAdView());
                break;

            case ITEM_VIEW_TYPE:
            default:
                Weather itemWeather = (Weather) weatherList.get(position);

                SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);
                //create element variables
                //temp
                float temp= UnitConvertor.convertTemp(Float.parseFloat(itemWeather.getTemperature()),sp);
                if (sp.getBoolean(Constants.sp_tempInteger,false)) {
                    temp = Math.round(temp);
                }

                //Rain
                double rain= Double.parseDouble(itemWeather.getRain());
                String rainString = UnitConvertor.getRainString(rain,sp);

                //Wind
                double wind;
                try {
                    wind = Double.parseDouble(itemWeather.getWind());
                } catch (Exception e) {
                    e.printStackTrace();
                    wind=0;
                }
                wind = UnitConvertor.convertWind(wind,sp);

                //Pressure
                double pressure = UnitConvertor.convertPressure((float) Double.parseDouble(itemWeather.getPressure()),sp);

                String dDateFormat = context.getResources().getStringArray(R.array.dateFormatsValues)[0];
                String dateString;
                try {
                    SimpleDateFormat simpleDateFormat= new SimpleDateFormat(dDateFormat);
                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    dateString = simpleDateFormat.format(itemWeather.getDate());
                } catch (IllegalArgumentException e) {
                    dateString = context.getResources().getString(R.string.error_dateFormat);
                }

                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                //set viewholder views
                itemViewHolder.itemDate.setText(dateString);
                itemViewHolder.itemTemp.setText(new DecimalFormat(Constants.sp_decimal_format0x).format(temp)+" "+sp.getString(Constants.sp_unit,Constants.sp_default_unit));
                itemViewHolder.itemDescrip.setText(itemWeather.getDescribtion().substring(0,1).toUpperCase()+itemWeather.getDescribtion().substring(1)+rainString);
                itemViewHolder.itemIcon.setText(itemWeather.getIcon());
                if (sp.getString(Constants.sp_speed_unit,Constants.sp_default_speed_unit).equals(Constants.sp_speed_unit_bft)) {
                    itemViewHolder.itemWind.setText(context.getString(R.string.wind)+": "+ UnitConvertor.getBeaufortName((int)wind,context)+" "
                            + Format.getWindDirectionString(sp,context,itemWeather));
                }
                else {
                    itemViewHolder.itemWind.setText(context.getString(R.string.wind)+": "+ new DecimalFormat("0.0").format(pressure)+" "
                            +MainActivity.localize(sp,context,Constants.sp_speed_unit,Constants.sp_default_speed_unit
                            + Format.getWindDirectionString(sp,context,itemWeather)));
                }

                itemViewHolder.itemPressure.setText(context.getString(R.string.pressure)+": "+new DecimalFormat("0.0").format(pressure)
                        +" " + MainActivity.localize(sp,context,Constants.sp_pressure_unit,Constants.sp_default_pressure_unit));
                itemViewHolder.itemHumid.setText(context.getString(R.string.humidity)+": "+itemWeather.getHumidity()+Constants.sp_default_humid_unit);
        }
    }

    private void parseNativeAdView (UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        ((TextView)adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView)adView.getBodyView()).setText(nativeAd.getBody());
        ((Button)adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        NativeAd.Image icon = nativeAd.getIcon();
        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        }
        else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice()== null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        }
        else {
            ((TextView)adView.getPriceView()).setText(nativeAd.getPrice());
            adView.getPriceView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        adView.setNativeAd(nativeAd);
    }

    @Override
    public int getItemViewType(int position) {
        Object ItemView = weatherList.get(position);
            if (ItemView instanceof UnifiedNativeAd) {
                return NATIVE_ADS_VIEW_TYPE;
            }
            return ITEM_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView itemDate;
        TextView itemTemp;
        TextView itemDescrip;
        TextView itemWind;
        TextView itemPressure;
        TextView itemHumid;
        TextView itemIcon;
        View lineView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            itemDate = itemView.findViewById(R.id.itemDate);
            itemTemp = itemView.findViewById(R.id.itemTemp);
            itemDescrip = itemView.findViewById(R.id.itemDescrib);
            itemWind = itemView.findViewById(R.id.itemWind);
            itemPressure =itemView.findViewById(R.id.itemPressure);
            itemHumid = itemView.findViewById(R.id.itemHumid);
            itemIcon = itemView.findViewById(R.id.itemIcon);
            lineView = itemView.findViewById(R.id.divider);
        }
    }
}
