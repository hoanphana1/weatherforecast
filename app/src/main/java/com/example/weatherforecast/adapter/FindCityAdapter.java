package com.example.weatherforecast.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.R;
import com.example.weatherforecast.constant.Constants;
import com.example.weatherforecast.model.Weather;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class FindCityAdapter extends RecyclerView.Adapter <FindCityAdapter.ViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Weather> weatherArrayList;
    ItemClickListener itemClickListener;

    public FindCityAdapter(Context context, ArrayList<Weather> weatherArrayList, ItemClickListener itemClickListener) {
        this.context = context;
        this.weatherArrayList = weatherArrayList;
        this.layoutInflater = LayoutInflater.from(context);
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= layoutInflater.inflate(R.layout.find_city_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Weather weather= weatherArrayList.get(position);

        holder.txtCity.setText(weather.getCity()+" "+weather.getCountry());
        holder.txtTemp.setText(weather.getTemperature());
        holder.txtIcon.setText(weather.getIcon());
        holder.txtDescrip.setText(weather.getDescribtion());
        holder.doOnmapReady();
    }

    @Override
    public int getItemCount() {
        return weatherArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, OnMapReadyCallback {

        TextView txtCity,
                txtTemp,
                txtDescrip,
                txtIcon;
        MapView mapView;
        GoogleMap map;

        public ViewHolder(@NonNull View itemView)  {
            super(itemView);

            txtCity= itemView.findViewById(R.id.FCCityTextView);
            txtDescrip = itemView.findViewById(R.id.FCDescripTextView);
            txtIcon = itemView.findViewById(R.id.FcIconTextView);
            mapView = itemView.findViewById(R.id.FCMapView);
            txtTemp = itemView.findViewById(R.id.FCtempTextView);
            if (mapView!=null) {
                mapView.onCreate(null);
                mapView.getMapAsync(this);
            }

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.OnItemClickListener(v,getAdapterPosition());
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context);
            map=googleMap;
            doOnmapReady();
//            LatLng location = new LatLng(weatherArrayList.get(getAdapterPosition()).getLat(),weatherArrayList.get(getAdapterPosition()).getLon());
//            map.addMarker(new MarkerOptions().position(location));
//            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, Constants.map_zoomlevel));
        }
       private void  doOnmapReady() {
            if (map==null) return;
            LatLng location = new LatLng(weatherArrayList.get(getAdapterPosition()).getLat(),weatherArrayList.get(getAdapterPosition()).getLon());
           UiSettings settings = map.getUiSettings();
           settings.setZoomControlsEnabled(true);
           map.addMarker(new MarkerOptions().position(location));
           map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, Constants.map_zoomlevel));
           map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
       }
    }

    public Weather getItem(int position) {
        return weatherArrayList.get(position);
    }

    public interface ItemClickListener {
        void OnItemClickListener(View view,int position);
    }
}
