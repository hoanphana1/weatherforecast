package com.example.weatherforecast.adapter;

import android.nfc.tech.NfcA;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.R;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

public class NativeAdsViewHolder extends RecyclerView.ViewHolder {

    private UnifiedNativeAdView NativeAdView;

    public UnifiedNativeAdView getNativeAdView() {
        return NativeAdView;
    }

    public NativeAdsViewHolder(@NonNull View itemView) {
        super(itemView);
        NativeAdView = itemView.findViewById(R.id.native_ad_view);
        NativeAdView.setMediaView(NativeAdView.findViewById(R.id.ad_media));
        NativeAdView.setHeadlineView(NativeAdView.findViewById(R.id.ad_headline));
        NativeAdView.setBodyView(NativeAdView.findViewById(R.id.ad_body));
        NativeAdView.setCallToActionView(NativeAdView.findViewById(R.id.ad_button));
        NativeAdView.setIconView(NativeAdView.findViewById(R.id.ad_icon));
        NativeAdView.setPriceView(NativeAdView.findViewById(R.id.ad_price));
        NativeAdView.setStarRatingView(NativeAdView.findViewById(R.id.ad_rating));
        NativeAdView.setStoreView(NativeAdView.findViewById(R.id.ad_store));
        NativeAdView.setAdvertiserView(NativeAdView.findViewById(R.id.ad_advertiser));
    }
}
